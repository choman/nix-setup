#!/usr/bin/env bash
#
DOTFILES="https://github.com/choman/dotfiles"
BOOTSTRAP=true


error_exit() {
   msg=$1
   echo $1
   exit 1
}

source /etc/os-release
if ! sudo -v; then
   error_exit "Need to be able to sudo for the initial install"
fi

if ! which curl; then
   sudo apt install -y curl
fi

# install nix
sh <(curl -L https://nixos.org/nix/install) --no-daemon

NIX_PROFILE="${HOME}/.nix-profile/etc/profile.d/nix.sh"
if [[ ! -f  "${NIX_PROFILE}" ]]; then 
   error_exit "Nix did not install correctly"
fi
	
echo "sourcing nix profile: ${NIX_PROFILE}"
eval "$(cat ${NIX_PROFILE})"
#source ${NIX_PROFILE}


# Setup unstable 
echo "setup initial channels"
nix-channel --add https://nixos.org/channels/nixpkgs-unstable nixpkgs
nix-channel --update
nix-env -iA nixpkgs.nix-info nixpkgs.yadm

if ! which nix-info; then
   error_exit "Install failed???"
fi

nix-info -m
echo "Cloning: ${DOTFILES}"
pushd ${HOME}
if BOOTSTRAP; then 
   yadm clone --bootstrap $DOTFILES
else
   yadm clone --no-bootstrap $DOTFILES
fi

ls -la ${HOME}/.config/home-manager/
[[ -f "${HOME}/.config/home-manager/home.nix\#\#template.default" ]] && echo "HELLZ YEAH" || echo "FUUUUUUUCK"

nix-channel --add https://github.com/nix-community/home-manager/archive/master.tar.gz home-manager
nix-channel --update
nix-shell '<home-manager>' -A install

for i in .bashrc .profile
do
    if yadm status | grep $i; then
        yadm restore $i
    fi
done
    

yadm alt
nix-env -e nix-info yadm
nix-env -iA nixpkgs.home-manager

home-manager switch
source ${HOME}/.bashrc
